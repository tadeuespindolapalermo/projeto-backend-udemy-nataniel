package br.com.projetobackend.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.projetobackend.entity.Role;
import br.com.projetobackend.entity.User;
import br.com.projetobackend.repository.RoleRepository;
import br.com.projetobackend.repository.UserRepository;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		List<User> users = userRepository.findAll();
		
		if(users.isEmpty()) {
			
			createUser("Tadeu Espíndola Palermo", 
					"tadeupalermoti@gmail.com", 
					passwordEncoder.encode("123456"), "ROLE_ALUNO");
			
			createUser("Tadeu Espíndola Palermo", 
					"admin@gmail.com", 
					passwordEncoder.encode("admin"), "ROLE_ADMIN");			
			
		}		
	}
	
	public void createUser(String name, String email, String password, String role) {
		
		Role roleObj = new Role();
		roleObj.setName(role);
		roleRepository.save(roleObj);
		
		User user = new User(name, email, password, Arrays.asList(roleObj));
		userRepository.save(user);
	}

}
